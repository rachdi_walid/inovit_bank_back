"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.connectDB = exports.client = void 0;
const pg_1 = require("pg");
// A new client instance
const client = new pg_1.Client({
    user: "postgres",
    password: "root",
    host: "localhost",
    port: 5432,
    database: "inovit_bank",
});
exports.client = client;
//  Connect to the database
function connectDB() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield client.connect();
            console.log("Ahoy! Connected to the database!");
        }
        catch (error) {
            console.error("Shiver me timbers! Error connecting to the database:", error);
        }
    });
}
exports.connectDB = connectDB;
