"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const db_1 = require("./db");
const router = express_1.default.Router();
/***************************************************************************************************************************/
// Deposit route
router.post("/deposit", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // Validate request body
    const { accountId, amount } = req.body;
    if (!accountId || !amount) {
        return res.status(400).json({ error: "Invalid request body" });
    }
    // Connect to the database
    yield (0, db_1.connectDB)();
    try {
        // Check if the account exists
        const account = yield db_1.client
            .query("SELECT * FROM bank_account WHERE id = $1", [accountId])
            .then((result) => result.rows[0]);
        if (!account) {
            return res.status(404).json({ error: "Account not found" });
        }
        // Perform the deposit transaction
        const updatedBalance = account.balance + amount;
        yield db_1.client.query("UPDATE bank_account SET balance = $1 WHERE id = $2", [
            updatedBalance,
            accountId,
        ]);
        // Insert the transaction record
        const transaction = {
            id: 123,
            account_id: accountId,
            time_stamp: new Date(),
            amount,
            balance: updatedBalance,
            description: "Deposit",
        };
        yield db_1.client.query("INSERT INTO transaction ( account_id, time_stamp, amount, balance, description) VALUES ($1, $2, $3, $4, $5)", [
            transaction.account_id,
            transaction.time_stamp,
            transaction.amount,
            transaction.balance,
            transaction.description,
        ]);
        res.sendStatus(200);
    }
    catch (error) {
        console.error("Error executing deposit:", error);
        res.sendStatus(500);
    }
}));
/***************************************************************************************************************************/
// Withdraw route
router.post("/withdraw", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // Validate request body
    const { accountId, amount } = req.body;
    if (!accountId || !amount) {
        return res.status(400).json({ error: "Invalid request body" });
    }
    // Connect to the database
    yield (0, db_1.connectDB)();
    try {
        // Check if the account exists
        const account = yield db_1.client
            .query("SELECT * FROM bank_account WHERE id = $1", [accountId])
            .then((result) => result.rows[0]);
        if (!account) {
            return res.status(404).json({ error: "Account not found" });
        }
        // Check if the withdrawal exceeds the overdraft threshold
        const availableBalance = account.balance || 0 + account.credit_line;
        if (amount > availableBalance) {
            return res
                .status(400)
                .json({ error: "Insufficient funds. Time to find a money tree! 🌳" });
        }
        // Perform the withdrawal transaction
        const updatedBalance = account.balance || 0 - amount;
        yield db_1.client.query("UPDATE bank_account SET balance = $1 WHERE id = $2", [
            updatedBalance,
            accountId,
        ]);
        // Insert the transaction record
        const transaction = {
            id: 123,
            account_id: accountId,
            time_stamp: new Date(),
            amount: -amount,
            balance: updatedBalance,
            description: "Withdrawal",
        };
        yield db_1.client.query("INSERT INTO transaction ( account_id, time_stamp, amount, balance, description) VALUES ($1, $2, $3, $4, $5)", [
            transaction.account_id,
            transaction.time_stamp,
            transaction.amount,
            transaction.balance,
            transaction.description,
        ]);
        res.sendStatus(200); // Success! 💸
    }
    catch (error) {
        console.error("Error executing withdrawal:", error);
        res.sendStatus(500);
    }
}));
/***************************************************************************************************************************/
// Transaction history route
router.get("/transactions", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // Validate request parameters
    const { accountId } = req.query;
    if (!accountId) {
        return res.status(400).json({ error: "Invalid request parameters" });
    }
    // Connect to the database
    yield (0, db_1.connectDB)();
    try {
        // Check if the account exists
        const account = yield db_1.client
            .query("SELECT * FROM bank_account WHERE id = $1", [accountId])
            .then((result) => result.rows[0]);
        if (!account) {
            return res.status(404).json({ error: "Account not found" });
        }
        // Fetch transaction history for the account
        const transactions = yield db_1.client
            .query("SELECT * FROM transaction WHERE account_id = $1", [accountId])
            .then((result) => result.rows);
        res.json(transactions);
    }
    catch (error) {
        console.error("Error retrieving transaction history:", error);
        res.sendStatus(500);
    }
}));
exports.default = router;
