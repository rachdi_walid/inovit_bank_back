"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router_1 = __importDefault(require("./router"));
const app = (0, express_1.default)();
const port = 3000;
// Middleware
app.use(express_1.default.json());
// Routes
app.use("/api", router_1.default);
// Error handling middleware
app.use((err, req, res) => {
    console.error("An error occurred:", err);
    res.status(500).json({ error: "Internal server error" });
});
// Start the server
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});
