import express, { Request, Response, raw } from "express";
import { client, connectDB } from "./db";
import Customer from "./models/customer"; // Import the Customer type
import BankAccount from "./models/bank_account"; // Import the BankAccount type
import Transaction from "./models/transaction"; // Import the Transaction type

const router = express.Router();

/***************************************************************************************************************************/

// Deposit route
router.post("/deposit", async (req: Request, res: Response) => {
  // Validate request body
  const { accountId, amount } = req.body;
  if (!accountId || !amount) {
    return res.status(400).json({ error: "Invalid request body" });
  }

  // Connect to the database
  await connectDB();

  try {
    // Check if the account exists
    const account: BankAccount | null = await client
      .query("SELECT * FROM bank_account WHERE id = $1", [accountId])
      .then((result) => result.rows[0]);
    if (!account) {
      return res.status(404).json({ error: "Account not found" });
    }

    // Perform the deposit transaction
    const updatedBalance = account.balance + amount;
    await client.query("UPDATE bank_account SET balance = $1 WHERE id = $2", [
      updatedBalance,
      accountId,
    ]);

    // Insert the transaction record
    const transaction: Transaction = {
      id: 123, //I don't use this id when in sert the row in th table
      account_id: accountId,
      time_stamp: new Date(),
      amount,
      balance: updatedBalance,
      description: "Deposit",
    };
    await client.query(
      "INSERT INTO transaction ( account_id, time_stamp, amount, balance, description) VALUES ($1, $2, $3, $4, $5)",
      [
        transaction.account_id,
        transaction.time_stamp,
        transaction.amount,
        transaction.balance,
        transaction.description,
      ]
    );

    res.sendStatus(200);
  } catch (error) {
    console.error("Error executing deposit:", error);
    res.sendStatus(500);
  }
});

/***************************************************************************************************************************/

// Withdraw route
router.post("/withdraw", async (req: Request, res: Response) => {
  // Validate request body
  const { accountId, amount } = req.body;
  if (!accountId || !amount) {
    return res.status(400).json({ error: "Invalid request body" });
  }

  // Connect to the database
  await connectDB();

  try {
    // Check if the account exists
    const account: BankAccount | null = await client
      .query("SELECT * FROM bank_account WHERE id = $1", [accountId])
      .then((result) => result.rows[0]);
    if (!account) {
      return res.status(404).json({ error: "Account not found" });
    }

    // Check if the withdrawal exceeds the overdraft threshold
    const availableBalance = account.balance || 0 + account.credit_line;
    if (amount > availableBalance) {
      return res
        .status(400)
        .json({ error: "Insufficient funds. Time to find a money tree! 🌳" });
    }

    // Perform the withdrawal transaction
    const updatedBalance = account.balance || 0 - amount;
    await client.query("UPDATE bank_account SET balance = $1 WHERE id = $2", [
      updatedBalance,
      accountId,
    ]);

    // Insert the transaction record
    const transaction: Transaction = {
      id: 123, //I don't use this id when in sert the row in th table
      account_id: accountId,
      time_stamp: new Date(),
      amount: -amount, // Let's subtract the amount for withdrawals, obviously!
      balance: updatedBalance,
      description: "Withdrawal",
    };
    await client.query(
      "INSERT INTO transaction ( account_id, time_stamp, amount, balance, description) VALUES ($1, $2, $3, $4, $5)",
      [
        transaction.account_id,
        transaction.time_stamp,
        transaction.amount,
        transaction.balance,
        transaction.description,
      ]
    );

    res.sendStatus(200); // Success! 💸
  } catch (error) {
    console.error("Error executing withdrawal:", error);
    res.sendStatus(500);
  }
});

/***************************************************************************************************************************/

// Transaction history route
router.get("/transactions", async (req: Request, res: Response) => {
  // Validate request parameters
  const { accountId } = req.query;
  if (!accountId) {
    return res.status(400).json({ error: "Invalid request parameters" });
  }

  // Connect to the database
  await connectDB();

  try {
    // Check if the account exists
    const account: BankAccount | null = await client
      .query("SELECT * FROM bank_account WHERE id = $1", [accountId])
      .then((result) => result.rows[0]);
    if (!account) {
      return res.status(404).json({ error: "Account not found" });
    }

    // Fetch transaction history for the account
    const transactions: Transaction[] = await client
      .query("SELECT * FROM transaction WHERE account_id = $1", [accountId])
      .then((result) => result.rows);

    res.json(transactions);
  } catch (error) {
    console.error("Error retrieving transaction history:", error);
    res.sendStatus(500);
  }
});

export default router;
