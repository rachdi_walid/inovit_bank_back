interface Customer {
  id: number;
  last_name: string | null;
  first_name: string | null;
  street: string | null;
  city: string | null;
  state: string | null;
  zip: number | null;
  phone: number | null;
  email: string | null;
}

export default Customer;
