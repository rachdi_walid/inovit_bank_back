interface BankAccount {
  id: number;
  type: string | null;
  description: string | null;
  balance: number | null;
  credit_line: number;
  created_at: Date | null;
}

export default BankAccount;
