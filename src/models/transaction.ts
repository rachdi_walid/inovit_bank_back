interface Transaction {
  id: number;
  account_id: number;
  time_stamp: Date | null;
  amount: number | null;
  balance: number | null;
  description: string | null;
}

export default Transaction;
