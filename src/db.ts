import { Client } from "pg";

// A new client instance
const client = new Client({
  user: "postgres",
  password: "root",
  host: "localhost",
  port: 5432,
  database: "inovit_bank",
});

//  Connect to the database
async function connectDB() {
  try {
    await client.connect();
    console.log("Ahoy! Connected to the database!");
  } catch (error) {
    console.error(
      "Shiver me timbers! Error connecting to the database:",
      error
    );
  }
}

export { client, connectDB };
