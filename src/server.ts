import express, { Request, Response } from "express";
import router from "./router";

const app = express();
const port = 3000;

// Middleware
app.use(express.json());

// Routes
app.use("/api", router);

// Error handling middleware
app.use((err: Error, req: Request, res: Response) => {
  console.error("An error occurred:", err);
  res.status(500).json({ error: "Internal server error" });
});

// Start the server
app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
