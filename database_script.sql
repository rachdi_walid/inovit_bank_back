--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3
-- Dumped by pg_dump version 15.3

-- Started on 2023-06-05 00:12:08

SET statement_timeout
= 0;
SET lock_timeout
= 0;
SET idle_in_transaction_session_timeout
= 0;
SET client_encoding
= 'UTF8';
SET standard_conforming_strings
= on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies
= false;
SET xmloption
= content;
SET client_min_messages
= warning;
SET row_security
= off;

--
-- TOC entry 3358 (class 1262 OID 16397)
-- Name: inovit_bank; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE inovit_bank WITH TEMPLATE = template0
ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'French_France.1252';


ALTER DATABASE inovit_bank OWNER TO postgres;

\connect inovit_bank

SET statement_timeout
= 0;
SET lock_timeout
= 0;
SET idle_in_transaction_session_timeout
= 0;
SET client_encoding
= 'UTF8';
SET standard_conforming_strings
= on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies
= false;
SET xmloption
= content;
SET client_min_messages
= warning;
SET row_security
= off;

SET default_tablespace
= '';

SET default_table_access_method
= heap;

--
-- TOC entry 219 (class 1259 OID 16410)
-- Name: bank_account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bank_account
(
    id integer NOT NULL,
    type text,
    description text,
    credit_line real,
    created_at date,
    balance real DEFAULT 0
);


ALTER TABLE public.bank_account OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16408)
-- Name: bank_account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bank_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.bank_account_id_seq OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16409)
-- Name: bank_account_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bank_account_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_account_id_seq1 OWNER TO postgres;

--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 218
-- Name: bank_account_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bank_account_id_seq1
OWNED BY public.bank_account.id;


--
-- TOC entry 216 (class 1259 OID 16400)
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer
(
    id integer NOT NULL,
    last_name text,
    first_name text,
    street text,
    city text,
    state text,
    zip integer,
    phone integer,
    email text
);


ALTER TABLE public.customer OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16418)
-- Name: customer_account_xref; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer_account_xref
(
    customer_id integer NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE public.customer_account_xref OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16398)
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.customer_id_seq OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16399)
-- Name: customer_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customer_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customer_id_seq1 OWNER TO postgres;

--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 215
-- Name: customer_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customer_id_seq1
OWNED BY public.customer.id;


--
-- TOC entry 224 (class 1259 OID 16424)
-- Name: transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transaction
(
    id integer NOT NULL,
    account_id integer NOT NULL,
    time_stamp date,
    amount real,
    balance real,
    description text
);


ALTER TABLE public.transaction OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16423)
-- Name: transaction_account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transaction_account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_account_id_seq OWNER TO postgres;

--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 223
-- Name: transaction_account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transaction_account_id_seq
OWNED BY public.transaction.account_id;


--
-- TOC entry 221 (class 1259 OID 16421)
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.transaction_id_seq OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16422)
-- Name: transaction_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transaction_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_id_seq1 OWNER TO postgres;

--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 222
-- Name: transaction_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transaction_id_seq1
OWNED BY public.transaction.id;


--
-- TOC entry 3192 (class 2604 OID 16413)
-- Name: bank_account id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_account
ALTER COLUMN id
SET
DEFAULT nextval
('public.bank_account_id_seq1'::regclass);


--
-- TOC entry 3191 (class 2604 OID 16403)
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
ALTER COLUMN id
SET
DEFAULT nextval
('public.customer_id_seq1'::regclass);


--
-- TOC entry 3194 (class 2604 OID 16427)
-- Name: transaction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaction
ALTER COLUMN id
SET
DEFAULT nextval
('public.transaction_id_seq1'::regclass);


--
-- TOC entry 3195 (class 2604 OID 16428)
-- Name: transaction account_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaction
ALTER COLUMN account_id
SET
DEFAULT nextval
('public.transaction_account_id_seq'::regclass);


--
-- TOC entry 3347 (class 0 OID 16410)
-- Dependencies: 219
-- Data for Name: bank_account; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.bank_account
    (id, type, description, credit_line, created_at, balance)
VALUES
    (1, 'Saving', 'For Vacation', -2000, '2023-06-02', 2000);


--
-- TOC entry 3344 (class 0 OID 16400)
-- Dependencies: 216
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.customer
    (id, last_name, first_name, street, city, state, zip, phone, email)
VALUES
    (1, 'RACHDI', 'Walid', '81 RUE d Angulême', 'Corbeil-Essonnes', 'FRANCE', 91100, 758726356, 'walid.rachdi@datarox.fr');


--
-- TOC entry 3348 (class 0 OID 16418)
-- Dependencies: 220
-- Data for Name: customer_account_xref; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.customer_account_xref
    (customer_id, account_id)
VALUES
    (1, 1);


--
-- TOC entry 3352 (class 0 OID 16424)
-- Dependencies: 224
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 217
-- Name: bank_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bank_account_id_seq', 1, false);


--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 218
-- Name: bank_account_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bank_account_id_seq1', 1, true);


--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 214
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customer_id_seq', 1, false);


--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 215
-- Name: customer_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customer_id_seq1', 1, true);


--
-- TOC entry 3367 (class 0 OID 0)
-- Dependencies: 223
-- Name: transaction_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transaction_account_id_seq', 1, false);


--
-- TOC entry 3368 (class 0 OID 0)
-- Dependencies: 221
-- Name: transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transaction_id_seq', 1, false);


--
-- TOC entry 3369 (class 0 OID 0)
-- Dependencies: 222
-- Name: transaction_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transaction_id_seq1', 1, false);


--
-- TOC entry 3199 (class 2606 OID 16417)
-- Name: bank_account bank_account_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_account
ADD CONSTRAINT bank_account_pk PRIMARY KEY
(id);


--
-- TOC entry 3197 (class 2606 OID 16407)
-- Name: customer customer_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
ADD CONSTRAINT customer_pk PRIMARY KEY
(id);


-- Completed on 2023-06-05 00:12:08

--
-- PostgreSQL database dump complete
--

